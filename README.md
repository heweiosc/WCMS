#WCMS  
#V9.3 推荐使用    
1、支持自定义url文件名  
2、不要禁用scandir函数  
3、改进错误提示  
4、大幅度提高了标签性能    

V9.2  　 
1.采用阿里IP方式  
2.采用文泉驿等宽字体  
3.CKEditor替换成Ueditor1.4.3       
4.路径问题都使用./ 列表页为动态  内容页会自动转化成../../  首页除外     
5.增加界面颜色自定义  引入了less  
6.增加水印上传  
7.修正摘要换行问题  
  
V9.1  
1.改进静态生成方式  
2.新增邮箱密码找回  
3.新增系统配置  
4.新增了QQ一键登录    
   
   
V9.0     
1.自定义多个标签 如面积.风格.颜色    
2.增加了通过标签搜索  
3.修改后台界面参考了phpcms  
4.后台可自行开启是否打水印和配置位置   
5.列表页参数 cid分类 sort排序 flag标签 num数量p当前分页    
6.不再支持列表页静态  
7.可以开启或者关闭分页数统计已提高性能  
8.支持二维码功能  

#1.环境配置：  
1.php_exif 图集批量上传所需  
2.php_pdo  需要pdo支持    
3./a 需要写入权限 内容静态页面    
4./config/database.php  需要写入权限  

#2.安装方法
1.运行根目录下的install.php文件即可  
2.后台访问地址：http://你的域名或IP/index.php?anonymous/login   
3.默认帐号 15800000000 密码123456
  
#3.配置文件说明  
config/database.php 数据库配置    
config/captcha.ttf 验证码字体文件（更改可直接替换文件）  

#4.前端访问地址    
专题页面  ./index.php?news/i/?cid=2  
列表页面 ./index.php?news/c/?cid=2  
内容页面 ./index.php?news/v/?id=2014  

#5.QQ群交流 425590782  

#6.演示地址  
1.[http://wcms.oschina.mopaas.com/](http://wcms.oschina.mopaas.com/)    
2.默认帐号 15800000000 密码123456

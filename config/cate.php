<?php

//后台添加内容模板定义  这里是分类模型属性定义 针对内容和分类划分
$newsConfig = array(
        1 => array(
                'name' => 'article',
                'add' => "news.add.tpl",
                'edit' => "news.edit.tpl",
                'list' => "list.tpl"),
        2 => array(
                'name' => 'image',
                'add',
                'edit',
                'list'),
        3 => array(
                'name' => 'special',
                'add',
                'edit',
                'list'),
        5 => array(
                'name' => 'system',
                'add',
                'edit',
                'list'));